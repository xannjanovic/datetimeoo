﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DateTimeOO
{
     class Months: BaseTimeUnit
    {
        int month;
        public int returnMonth()
        {
            return month;
        }

        public void incraseMonth()
        {
            month++;
            FireOnChange(month);
        }
    }
}
