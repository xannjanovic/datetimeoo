﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DateTimeOO
{
     class Minutes: BaseTimeUnit
    {
        int Minute;
        Seconds secundskis;

        public Minutes(Seconds secs)
        {
            if (secs == null)            
                throw new ArgumentNullException("Seconds are null");
            
            secundskis = secs;
            secundskis.OnReset += delegate() { increaseMinutes(); };

        }
        public int returnMinutes()
        {
            return Minute;
        }
        public void increaseMinutes()
        {   
            if(!resetMinutesIfNeeded())
               Minute++;        
    
            FireOnChange(Minute);
        }

        private bool resetMinutesIfNeeded()
        {
            bool IsResetNeeded = (Minute == 60);

            if (IsResetNeeded)
            {
                Minute = 0;
                FireOnReset();
            }
            return IsResetNeeded;
        }
    }
}
