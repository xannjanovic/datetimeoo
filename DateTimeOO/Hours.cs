﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DateTimeOO
{
     class Hours: BaseTimeUnit
    {

        int hour = 0;

        public Hours(Minutes MinObj)
        {
            if (MinObj == null)
                throw new ArgumentNullException("Minutes are null");
            Minutes Minuten = MinObj;
            
            Minuten.OnReset += delegate() { increaseHours(); };
        }
        
        public int returnHours()
        {
            return hour;
        }
        public void increaseHours()
        {
            if (!resetHoursIfNeeded())                      
                hour++;
            
            FireOnChange(hour);
        }
        private bool resetHoursIfNeeded()
        {
            bool IsResetNeeded = (hour == 24);

            if (IsResetNeeded)
            {
                hour = 0;
                FireOnReset();
            }
            return IsResetNeeded;
        }


    }
}
