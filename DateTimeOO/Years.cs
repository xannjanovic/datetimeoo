﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DateTimeOO
{
     class Years: BaseTimeUnit
    {
        int Year;
        public int returnYear()
        {
            return Year;
        }
        public void increaseYear()
        {
            Year++;
            FireOnChange(Year);
        }
    }
}
