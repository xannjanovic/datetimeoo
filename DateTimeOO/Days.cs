﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DateTimeOO
{
    class Days: BaseTimeUnit
    {
        int day;

        public int returnday()
        {
            return day;
        }
        public void increaseDay()
        {
            day++;
            FireOnChange(day);
        }
    }
}
