﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DateTimeOO
{
    class BaseTimeUnit
    {
        public delegate void OnChangeEvent(int timeUnitValue); //Deklararation der Methodensignatur (welche parameter) für die Regestrierung am Event
        public delegate void OnResetEvent();
        public event OnChangeEvent OnChange;
        public event OnResetEvent OnReset;  //Eigentliche Regestrierung am Event

        protected void FireOnChange(int timeUnitValue)
        {
            if (OnChange != null)
                OnChange(timeUnitValue);            
        }
        protected void FireOnReset()
        {
            if (OnReset != null)
                OnReset();
        }

    }
}
