﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using DateTimeOO;

namespace DateTimeOOTests
{
    [TestFixture]
    class MinutesTest
    {
        //Deklaration
        Minutes MinutesObj;
        Seconds SecondsObj;
        [SetUp]
        public void setup()
        {
            SecondsObj = new Seconds();
            //Instanziierung
            MinutesObj = new Minutes(SecondsObj);
        }

        [Test]
        public void TestMinuteAfterCreateIsZero()
        {
            Assert.AreEqual(0, MinutesObj.returnMinutes());
        }
        [Test]
        public void TestMinuteIsIncreasedbyOne()
        {
            MinutesObj.increaseMinutes();
            Assert.AreEqual(1, MinutesObj.returnMinutes());
            MinutesObj.increaseMinutes();
            Assert.AreEqual(2, MinutesObj.returnMinutes());
        }
        [Test]
        public void TestIfOnChangeIsFiredAfterIncrease()
        {
            MinutesObj.OnChange += delegate(int mins) { Assert.Pass(); }; // Anonyme Methode (Fachbegriff: Closure), da sie keinen namen hat. Registrierung der Methode an dem Event.
            MinutesObj.increaseMinutes();                                 // Aufrufung der Methode und dadurch feuern des Events.
            Assert.Fail();
        }

        [Test]
        public void TestIfOnChangeMatchesIncreasedValue()
        {
            MinutesObj.OnChange += delegate(int mins)
            {
                Assert.AreEqual(1, mins);
                Assert.Pass();
            };
            MinutesObj.increaseMinutes();
            Assert.Fail();
        }

        [Test]
        public void TestPassingNullToConstructorThrowsArguemntNullException()
        {
            Seconds sec = null;
            Assert.Throws<ArgumentNullException>(
                delegate()
                {
                    Minutes min = new Minutes(sec);
                }
                ); 
        }
        [Test]
        public void TestIfMinutesIncreaseWhenSecondsFireReset()
        {
            for (int i = 0; i < 61; i++)
            {
                SecondsObj.increaseSecond();
            }
            Assert.AreEqual(1, MinutesObj.returnMinutes());
            
        }

        [Test]
        public void TestIfMinutesResetIfTheyAre60()
        {
            SetupMinutesTo60();
            MinutesObj.increaseMinutes();
            Assert.AreEqual(0, MinutesObj.returnMinutes());
            
        }

        private void SetupMinutesTo60()
        {
            for (int i = 0; i < 60; i++)
			{
                MinutesObj.increaseMinutes();
			}
            Assert.AreEqual(60, MinutesObj.returnMinutes());
        }
    }
}
