﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using DateTimeOO;

namespace DateTimeOOTests.UnitTests
{
    [TestFixture]
    public class HoursTest
    {
        Hours hourObj;
        Minutes minObj;
        Seconds secObj;

        private void SetupHoursTo24()
        {
            for (int i = 0; i < 24; i++)
            {
                hourObj.increaseHours();
            }

            Assert.AreEqual(24, hourObj.returnHours());
        }
            private void SetupMinutesTo60()
            {
            
                for (int i = 0; i < 60; i++)
            {
                minObj.increaseMinutes();
            }

            Assert.AreEqual(60, minObj.returnMinutes());
        }
        [SetUp]
        public void Setup()
        {
            secObj = new Seconds();
            minObj = new Minutes(secObj);
            hourObj = new Hours(minObj);
            
        }

        [Test]
        public void TestAreHoursInitializedWith0()
        {
            Assert.AreEqual(0, hourObj.returnHours());
        }

        [Test]
        public void TestAreHoursIncreasedByOne()
        {            
            hourObj.increaseHours();
            Assert.AreEqual(1, hourObj.returnHours());
            hourObj.increaseHours();
            Assert.AreEqual(2, hourObj.returnHours());
        }
        [Test]
        public void TestIfOnChangeIsFiredAfterIncrease()
        {
            hourObj.OnChange += delegate(int hours) { Assert.Pass(); };
            hourObj.increaseHours();                                 
            Assert.Fail();
        }

        [Test]
        public void TestIfOnChangeMatchesIncreasedValue()
        {
            hourObj.OnChange += delegate(int hours)
            {
                Assert.AreEqual(1, hours);
                Assert.Pass();
            };
            hourObj.increaseHours();
            Assert.Fail();
        }

        [Test]
        public void TestIfHoursResetIfTheyAreAt24()
        {
            SetupHoursTo24();
            hourObj.increaseHours();
            Assert.AreEqual(0, hourObj.returnHours());
        }

        [Test]
        public void TestIfOnResetIsFiredIfHoursAreReseted()
        {
            SetupHoursTo24();
            hourObj.OnReset += delegate() 
            {
                Assert.AreEqual(0, hourObj.returnHours());
                Assert.Pass();
            };
            
            hourObj.increaseHours();
            Assert.AreEqual(0, hourObj.returnHours());
            Assert.Fail();
        }
       
        [Test]
        public void TestIfOnResetIsOnlyFiredIfHoursAre24()
        {            
            hourObj.OnReset += delegate()
            {
                Assert.AreEqual(0, hourObj.returnHours());
                Assert.Pass();
            };

            SetupHoursTo24();
            hourObj.increaseHours();
        }

        [Test]
        public void TestIfHoursAreIncreasedByOneIfMinutesAreSixty()
        {
            SetupMinutesTo60();
            minObj.increaseMinutes();
            Assert.AreEqual(1, hourObj.returnHours());

        }


    }


}
