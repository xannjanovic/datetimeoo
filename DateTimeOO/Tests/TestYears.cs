﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using DateTimeOO;

namespace DateTimeOOTests
{
    [TestFixture]
    class TestYears
    {
        Years yearObj;

        [SetUp]
        public void Setup()
        {
            yearObj = new Years();
        }

        [Test]
        public void TestIfYearIsZeroAfterCreate()
        {
            Assert.AreEqual(0, yearObj.returnYear());
        }

        [Test]
        public void TestIfYearIsIncreasedByOne()
        {
            yearObj.increaseYear();
            Assert.AreEqual(1, yearObj.returnYear());
        }
        [Test]
        public void TestIfOnChangeIsFiredAfterIncrease()
        {
            yearObj.OnChange += delegate(int year) { Assert.Pass(); };
            yearObj.increaseYear();
            Assert.Fail();
        }

        [Test]
        public void TestIfOnChangeMatchesIncreasedValue()
        {
            yearObj.OnChange += delegate(int year)
            {
                Assert.AreEqual(1, year);
                Assert.Pass();
            };
            yearObj.increaseYear();
            Assert.Fail();
        }

    }
}
