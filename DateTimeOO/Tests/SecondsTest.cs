﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using NUnit.Framework;
using DateTimeOO;

namespace DateTimeOOTests
{
    [TestFixture]
    class SecondsTest
    {
        Seconds SecondObj; //Deklariung wenn erst im Setup, lebt es nur in Setup        

        [SetUp]
        public void setup()
        {
            SecondObj = new Seconds(); //Instanzierung            
        }

        [Test]
        public void TestIfSecondIsZeroAfterInitilize()
        {
            Assert.AreEqual(0, SecondObj.returnSecond());
        }

        [Test]
        public void TestIfSecondIsIncreasedByOne()
        {
            int oldSeconds = SecondObj.returnSecond();
            SecondObj.increaseSecond();
            Assert.AreEqual(oldSeconds + 1 , SecondObj.returnSecond());
        }

        [Test]
        public void TestIfOnChangeIsFiredAfterIncrease()
        {
            SecondObj.OnChange += delegate(int secs){Assert.Pass();};
            SecondObj.increaseSecond();
            Assert.Fail();
        }

        [Test]
        public void TestIfOnChangeMatchesIncreasedValue()
        {
            SecondObj.OnChange += delegate(int secs) {
                Assert.AreEqual(1, secs);
                Assert.Pass();
            };
            SecondObj.increaseSecond();
            Assert.Fail();
        }
        
        [Test]
        public void TestIfOnResetIsFiredWhenSecondsAreIncreasedFrom60()
        {
            while (SecondObj.returnSecond() < 60) { SecondObj.increaseSecond(); }

            SecondObj.OnReset += delegate() //Regestriert und Deklariert die Methode an dem Event
            {   
                Assert.Pass();
            };

            Assert.AreEqual(60, SecondObj.returnSecond());
            SecondObj.increaseSecond();
            Assert.Fail("event wurde nicht gefeuert");
        }

        [Test]
        public void TestIfOnResetIsNotFiredWhenSecondsAreBetween0and59()
        {
            SecondObj.OnReset += delegate()
            {   
                Assert.Fail("event wurde gefeuert");
            };

            while (SecondObj.returnSecond() < 59) { SecondObj.increaseSecond(); }
            Assert.Pass();
        }
    }
}
