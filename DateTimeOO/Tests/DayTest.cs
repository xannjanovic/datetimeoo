﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using DateTimeOO;

namespace DateTimeOOTests
{
    [TestFixture]

    class DayTest
    {
        Days DayObj;

        [SetUp]
        public void Setup()
        {
            DayObj = new Days();
        }

        [Test]
        public void TestDayIfZeroAfterCreate()
        {
            Assert.AreEqual(0, DayObj.returnday());
        }
        [Test]
        public void TestIfDayIsIncreasedByOne()
        {
            DayObj.increaseDay();
            Assert.AreEqual(1, DayObj.returnday());
        }
        [Test]
        public void TestIfOnChangeIsFiredAfterIncrease()
        {
            DayObj.OnChange += delegate(int days) { Assert.Pass(); };
            DayObj.increaseDay();
            Assert.Fail();
        }

        [Test]
        public void TestIfOnChangeMatchesIncreasedValue()
        {
            DayObj.OnChange += delegate(int days)
            {
                Assert.AreEqual(1, days);
                Assert.Pass();
            };
            DayObj.increaseDay();
            Assert.Fail();
        }
    }
}
