﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using DateTimeOO;

namespace DateTimeOOTests
{
     [TestFixture]
    class MonthTest
    {
        Months monthObj;

        [SetUp]
        public void Setup()
        {
            monthObj = new Months();

        }

        [Test]
         public void TestIfMonthAfterCreateIsZero()
        {
            Assert.AreEqual(0, monthObj.returnMonth());
        }

         [Test]
        public void TestIfMonthIsIncreasedByOne()   
        {
            monthObj.incraseMonth();
            Assert.AreEqual(1, monthObj.returnMonth());
        }
         [Test]
         public void TestIfOnChangeIsFiredAfterIncrease()
         {
             monthObj.OnChange += delegate(int moths) { Assert.Pass(); };
             monthObj.incraseMonth();
             Assert.Fail();
         }

         [Test]
         public void TestIfOnChangeMatchesIncreasedValue()
         {
             monthObj.OnChange += delegate(int moths)
             {
                 Assert.AreEqual(1, moths);
                 Assert.Pass();
             };
             monthObj.incraseMonth();
             Assert.Fail();
         }
    }
}
